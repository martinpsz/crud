import React from 'react';
import Head from '../src/components/Head'
import Tasks from './components/Tasks'

function App() {
  return (
    <div className="App container max-w-screen-md mx-auto md:mt-2">
      <Head/>
      <Tasks/>
    </div>
  );
}

export default App;
