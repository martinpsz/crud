import React from 'react'
import {Helmet} from 'react-helmet'


const Head = () => {
  return (
    <Helmet>
        <title>AFSCME - Sample Project</title>
        <meta name="description" content="Sample project from Martin showing knowledge of how to build a serverless React app."/>
    </Helmet>
  )
}

export default Head