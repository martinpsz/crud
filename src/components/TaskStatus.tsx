import React from 'react'

const TaskStatus = () => {
  return (
    <div className='flex flex-row '>
        <p className="pr-3">Current Status:</p>
        <label className="flex items-center"><input type="checkbox" 
                                                    className='form-radio mx-2 text-[#4EAA5C]'/>Pending</label>
        <label className="flex items-center"><input type="checkbox" 
                                                    className='form-radio mx-2 text-[#4EAA5C]'/>Reviewing</label>
        <label className="flex items-center"><input type="checkbox" 
                                                    className='form-radio mx-2 text-[#4EAA5C]'/>Completed</label>
    </div>
  )
}

export default TaskStatus