import React from 'react'
import { MinusIcon } from '@heroicons/react/24/solid'
import { ArrowPathIcon } from '@heroicons/react/24/solid'
import TaskStatus from '../components/TaskStatus'

interface TaskProps {
    title: string;
}

const Task: React.FC<TaskProps> = ({title}:TaskProps) => {
  return (
    <div className="mt-5" style={{'border': '1px solid var(--green)',  'borderRadius': '8px'}}>
        <div className="flex flex-row py-1 pl-4 justify-between" style={{'background': 'var(--green)', 
                                                                         'color': 'white', 'fontWeight': 300, 
                                                                      'fontSize': '1.25rem', 'borderTopLeftRadius': 'inherit',
                                                                      'borderTopRightRadius': 'inherit'}}>
            <h2>{title}</h2>
            <button className="flex flex-row items-center pr-4"><MinusIcon className="h-6 w-6 pr-2"/>Delete Task</button>
        </div>
        <div className="flex flex-row pl-4 py-2 justify-between" style={{'color': 'var(--green)', 'fontSize': '1.1rem'}}>
            <TaskStatus/>
            <button className="flex flex-row items-center pr-4"><ArrowPathIcon className="h-6 w-6 pr-2"/>Update</button>
        </div>
    </div>
  )
}

export default Task