import React from 'react'
import { PlusIcon } from '@heroicons/react/24/solid'
import { MinusIcon } from '@heroicons/react/24/solid'
import Task from '../components/Task'

const Tasks = () => {
  return (
    <div>
        <div style={{'background': 'var(--green)', 'color': 'white', 'borderRadius': '4px'}}>
            <h2 className="pl-3 text-center" style={{'fontWeight': 300, 'fontSize': '1.25rem'}}>TASK MANAGER</h2>
            <div className="flex space-x-3 justify-center mt-2 pb-2">
                <button className="flex p-1" style={{'backgroundColor': 'white', 'color': 'var(--green)', 'borderRadius': '4px'}}><PlusIcon className="h-6 w-6 pr-1"/>Add new task</button>
                <button className="flex p-1" style={{'backgroundColor': 'white', 'color': 'var(--green)', 'borderRadius': '4px'}}><MinusIcon className="h-6 w-6 pr-1"/>Delete all tasks</button>
            </div>
        </div>
        <Task title="Sample Task"/>
        <Task title="Sample Task"/>
        <Task title="Sample Task"/>
        <Task title="Sample Task"/>
    </div>
  )
}

export default Tasks